# Writing clean and Testable code

This training is focused on writing better code. The training is aimed at junior to medior software developers, and for everyone who regularly writes code (test automation engineers, for example).

## Presentation

The presentation can be found in the folder `presentation`. Use the README there to set it up.
